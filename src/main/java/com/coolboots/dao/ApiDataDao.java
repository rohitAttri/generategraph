package com.coolboots.dao;

import java.util.List;

import com.coolboots.dto.ApiDataDto;

public interface ApiDataDao {

	List<ApiDataDto> getApiData(String type);

}
