package com.coolboots.dao;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

import com.coolboots.dto.ApiDataDto;

@Repository
public class ApiDataDaoImpl implements ApiDataDao {
	
	@Autowired
	private MongoTemplate primaryTemplate;
	
	@Override
	public List<ApiDataDto> getApiData(String type) {
		Query query = new Query(Criteria.where("type").is(type));
		return primaryTemplate.find(query, ApiDataDto.class, "api_response_data");
		//return primaryTemplate.findAll(ApiDataDto.class, "api_response_data");
	}
	
}
