package com.coolboots.controller;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.coolboots.dao.ApiDataDao;
import com.coolboots.dto.ApiDataDto;

@RestController
public class ChartController {
	
	@Autowired
	ApiDataDao apidataDao;
	
	@PostMapping("/get-data")
    public ResponseEntity<Map<String, Integer>> getDataFromJson(@RequestBody String message) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(message);
        Map<String, Integer> graphData = new TreeMap<>();
        List<ApiDataDto> apiDataList = apidataDao.getApiData((String) json.get("type"));
        
        for(ApiDataDto item : apiDataList) {
        	graphData.put(String.valueOf(item.getTime()), (int) item.getResponseTime());
        }
//need to put the current time when the response occurred
//        graphData.put("10", 294);
//        graphData.put("11", 427);
//        graphData.put("12", 109);
//        graphData.put("13", 588);
        return new ResponseEntity<>(graphData, HttpStatus.OK);
    }
}
