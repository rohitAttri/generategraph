package com.coolboots.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ComponentScan("com.coolboots")
@SpringBootApplication
public class GenerateGraphApplication implements WebMvcConfigurer{

	public static void main(String[] args) {
		SpringApplication.run(GenerateGraphApplication.class, args);
	}


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("landing-page");
    }
}
