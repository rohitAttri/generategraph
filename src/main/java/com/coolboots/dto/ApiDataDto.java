package com.coolboots.dto;

public class ApiDataDto {
	private String url;
	private int status;
	private long responseTime;
	private int time;
	private String type;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return String.format("ApiDataDto [url=%s, status=%s, responseTime=%s, time=%s, type=%s]", url, status,
				responseTime, time, type);
	}

}
